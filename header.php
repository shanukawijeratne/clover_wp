<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.css">
    <?php wp_head(); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/db_access.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/api_access.js"></script>
    <script type="text/javascript">
        function execute_header_scripts() {
            change_page('contact');
            set_api_keys();
            nav_categories()
        }

        var endpoint = '<?php echo admin_url('admin-ajax.php');?>';

        async function set_api_keys() {
            var key = await get_key_value('KEY', endpoint);
            var token = await get_key_value('TOKEN', endpoint);
            api_setup_shop_id(key.data);
            api_setup_shop_token(token.data);
        }

        async function nav_categories() {
            var cates = await fetch_categories();
            console.log(cates);
            for (let i = 0; i < cates.elements.length; i++) {
                console.log(cates.elements[i].name);
                $('#cates').append('<li><a class="dropdown-link">' +
                    cates.elements[i].name
                    + '</a></li>'
                )
            }
        }
    </script>
    <header class="header-part">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <ul class="header-info">
                        <li><i class="fas fa-envelope"></i>
                            <p>info@vegana.com</p></li>
                        <li><i class="fas fa-phone-alt"></i>
                            <p>+12027953213</p></li>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="header-option">
                        <div class="header-curr"><i class="fas fa-money-bill"></i><select
                                    class="header-select custom-select">
                                <option class="clr" selected>currency</option>
                                <option class="clr" value="1">Dollers</option>
                                <option class="clr" value="2">Pound</option>
                                <option class="clr" value="3">Euro</option>
                            </select></div>
                        <div class="header-lang"><i class="fas fa-globe"></i><select
                                    class="header-select custom-select">
                                <option class="clr" selected>language</option>
                                <option class="clr" value="1">English</option>
                                <option class="clr" value="2">Turkish</option>
                                <option class="clr" value="3">italian</option>
                            </select></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <nav class="navbar-part">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-element">
                        <ul class="left-widget">
                            <li><a class="icon icon-inline menu-bar" href="#"><i class="fas fa-align-left"></i></a></li>
                        </ul>
                        <a class="navbar-logo" href="#"><img
                                    src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"></a>
                        <form class="search-form navbar-src"><input type="text" placeholder="Search anything...">
                            <button class="btn btn-inline"><i class="fas fa-search"></i><span>search</span></button>
                        </form>
                        <ul class="right-widget">
                            <li><a class="icon icon-inline" onclick="change_page('login')" style="cursor: pointer"><i
                                            class="fas fa-user" style="margin-top: 35%"></i></a></li>
                            <li><a class="icon icon-inline" onclick="change_page('wish')" style="cursor: pointer"><i
                                            class="fas fa-heart" style="margin-top: 35%"></i><sup>0</sup></a></li>
                            <li><a class="icon icon-inline" onclick="change_page('cart')" style="cursor: pointer"><i
                                            class="fas fa-shopping-cart" style="margin-top: 35%"></i><sup>0</sup></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="navbar-slide">
                        <div class="navbar-content">
                            <div class="navbar-slide-cross"><a class="icon icon-inline cross-btn" href="#"><i
                                            class="fas fa-times"></i></a></div>
                            <div class="navbar-slide-logo"><a href="#"><img src="images/logo.png" alt="logo"></a></div>
                            <form class="search-form mb-4 navbar-slide-src"><input type="text"
                                                                                   placeholder="Search anything...">
                                <button class="btn btn-inline"><i class="fas fa-search"></i><span>search</span></button>
                            </form>
                            <ul class="navbar-list">
                                <li class="navbar-item"><a class="navbar-link" onclick="change_page('home')"
                                                           style="cursor: pointer">Home</a></li>
                                <li class="navbar-item navbar-dropdown"><a class="navbar-link dropdown-indicator"
                                                                           style="cursor: pointer"><span>Categories</span><i
                                                class="fas fa-chevron-down"></i></a>
                                    <ul class="dropdown-list" id="cates" style="overflow: scroll;height: 300px">

                                    </ul>
                                </li>
                                <li class="navbar-item"><a class="navbar-link"
                                                           onclick="change_page('products');execute_sale_page_scripts();"
                                                           style="cursor: pointer">Shop</a></li>
                                <li class="navbar-item"><a class="navbar-link" onclick="change_page('login')"
                                                           style="cursor: pointer">Track My Orders</a></li>
                                <li class="navbar-item"><a class="navbar-link" onclick="change_page('about')"
                                                           style="cursor: pointer">About Us</a></li>
                                <li class="navbar-item"><a class="navbar-link" onclick="change_page('contact')"
                                                           style="cursor: pointer">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="btmbar-part">
        <ul class="btmbar-widget">
            <li><a onclick="change_page('home')" style="cursor: pointer"><i
                            class="fas fa-home"></i><span>Home</span></a></li>
            <li><a href="wishlist.html" class="wish-icon"><i class="fas fa-heart"></i><span>Wishlist</span><sup>0</sup></a>
            </li>
            <li><a onclick="change_page('cart')" style="cursor: pointer" class="cart-icon"><i
                            class="fas fa-shopping-basket"></i><span>Cart</span><sup>0</sup></a></li>
            <li><a href="account.html"><i class="fas fa-cog"></i><span>Settings</span></a></li>
        </ul>
    </div>
</head>
<body onload="execute_header_scripts()">

