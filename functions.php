<?php
require get_template_directory() . '/includes/function-admin.php';

function load_css()
{
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('flaticon', get_template_directory_uri() . '/fonts/flaticon/flaticon.css', array(), false, 'all');
    wp_enqueue_style('flaticon');

    wp_register_style('slick', get_template_directory_uri() . '/css/vendor/slick.css', array(), true, 'all');
    wp_enqueue_style('slick');

    wp_register_style('main', get_template_directory_uri() . '/css/custom/main.css', array(), false, 'all');
    wp_enqueue_style('main');

    wp_register_style('index', get_template_directory_uri() . '/css/custom/index.css', array(), false, 'all');
    wp_enqueue_style('index');
}

add_action('wp_enqueue_scripts', 'load_css');

function load_js()
{
    wp_register_script('jquery', get_template_directory_uri() . '/js/vendor/jquery-1.12.4.min.js', 'jquery', false, true);
    wp_enqueue_script('jquery');

    wp_register_script('popper', get_template_directory_uri() . '/js/vendor/popper.min.js', 'jquery', false, true);
    wp_enqueue_script('popper');

    wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', 'jquery', false, true);
    wp_enqueue_script('bootstrap');

    wp_register_script('slick', get_template_directory_uri() . '/js/vendor/slick.min.js', 'jquery', false, true);
    wp_enqueue_script('slick');

    wp_register_script('customslick', get_template_directory_uri() . '/js/custom/slick.js', 'jquery', false, true);
    wp_enqueue_script('customslick');

    wp_register_script('main', get_template_directory_uri() . '/js/custom/main.js', 'jquery', false, true);
    wp_enqueue_script('main');

    wp_register_script('page', get_template_directory_uri() . '/js/page_control.js', 'jquery', false, true);
    wp_enqueue_script('page');

    wp_register_script('db_access', get_template_directory_uri() . '/js/db_access.js', 'jquery', false, true);
    wp_enqueue_script('db_access');

    wp_register_script('data_access', get_template_directory_uri() . '/js/api_access.js', 'jquery', false, true);
    wp_enqueue_script('data_access');
}

add_action('wp_enqueue_scripts', 'load_js');


//DATABASE ACTIONS

//SAVE KEY VALUES
add_action('wp_ajax_key_entry', 'key_entry_function');
add_action('wp_ajax_nopriv_key_entry', 'key_entry_function');
function key_entry_function()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "key_values";
    $charset_collate = $wpdb->get_charset_collate();
    if ($wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name) {
        $sql = "CREATE TABLE $table_name (
            ID mediumint(9) NOT NULL AUTO_INCREMENT,
            `name` text NOT NULL,
            `value` text NOT NULL,
            PRIMARY KEY  (ID)
    ) $charset_collate;";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    $name = $_POST['name'];
    $value = $_POST['value'];

    $rec = $wpdb->get_row("SELECT * FROM wp_key_values WHERE name = '$name';");
    if ($rec != null) {
        $wpdb->query("UPDATE wp_key_values SET value='$value' WHERE name='$name';");
    } else {
        $wpdb->insert("wp_key_values", array("name" => $name, "value" => $value));
    }

    wp_send_json_success($name);
}

//GET KEY VALUES
add_action('wp_ajax_key_retrieve', 'key_retrieve_function');
add_action('wp_ajax_nopriv_key_retrieve', 'key_retrieve_function');
function key_retrieve_function()
{
    global $wpdb;
    $name = $_POST['name'];
    $rec = $wpdb->get_row("SELECT * FROM wp_key_values WHERE name = '$name';");
    wp_send_json_success($rec);
}

?>