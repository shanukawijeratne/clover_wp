<script src="<?php echo get_template_directory_uri(); ?>/js/api_access.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/product-list-1.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/custom/price-range.js"></script>
<script>
    function execute_sale_page_scripts(){
        set_items();
        set_categories();
        set_tags();
    }
    async function set_items() {
        var res = await fetch_items();
        for (let i = 0; i < res.elements.length; i++) {
            console.log(res.elements[i])
            $('#shop_page_product_list').append('<div class="col-6 col-sm-6 col-md-4 col-lg-4">' +
                '<div class="product-card card-gape">' +
                '<div class="product-img">' +
                '<img src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg" alt="product">' +
                '<ul class="product-widget">' +
                '<ul class="product-widget">' +
                '<li><button><i class="fas fa-eye"></i></button></li>' +
                '<li><button><i class="fas fa-heart"></i></button></li>' +
                '<li><button><i class="fas fa-exchange-alt"></i></button></li>' +
                '</ul>' +
                '</div>' +
                '<div class="product-content">' +
                '<div class="product-name"><h6><a href="#">' + res.elements[i].name + '</a></h6></div>' +
                '<div class="product-price">' +
                '<h6><del>$80</del>$' + res.elements[i].price + '</h6>' +
                '<div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>' +
                '</div>' +
                '<div class="product-btn"><a href="#"><i class="fas fa-shopping-basket"></i><span>Add to Cart</span></a>' +
                '</div></div></div></div>'
            )
        }
    }

    async function set_categories() {
        var cates = await fetch_categories();
        for (let i = 0; i < cates.elements.length; i++) {
            $('#shop_page_cate_list').append('<li><a href="#"><span>' + cates.elements[i].name + '</span><i class="fa fa-arrow-right"></i></a>' +
                '<ul><li><a href="#">Demo item (0)</a></li><li><a href="#">Demo item (0)</a></li><li><a href="#">Demo item (0)</a></li></ul></li>'
            )
        }
    }

    async function set_tags() {
        console.log('tags')
        var tags = await fetch_tags();
        console.log(tags)
        for (let i = 0; i < tags.elements.length; i++) {
            $('#shop_page_tag_list').append('<li><div class="custom-control custom-checkbox">' +
                '<input type="checkbox" class="custom-control-input" id="check8">' +
                '<label class="custom-control-label" for="check8">Alcohol-Free</label></div>' +
                '<span class="product-cate-number">(13)</span></li>'
            )
        }
    }
</script>
<style>
    #shop_page_product_list::-webkit-scrollbar {
        width: 0 !important
    }

    #shop_page_product_list.element {
        overflow: -moz-scrollbars-none;
    }

    #shop_page_product_list.element {
        -ms-overflow-style: none;
    }
</style>
<section class="single-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-content"><h2>Product list with left sidebar</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Product-list-1</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="product-list">
    <div class="container">
        <div class="row content-reverse">
            <div class="col-lg-3">
                <div class="product-ad"><a href="#"><img
                                src="<?php echo get_template_directory_uri(); ?>/images/ad/01.jpg" alt="ad"></a></div>
                <div class="product-bar"><h6>Filter by tags</h6>
                    <div class="product-bar-content"><input type="text" placeholder="Search">
                        <ul class="scroll-list" id="shop_page_tag_list">

                        </ul>
                        <button class="clear-filter-btn"><i class="fas fa-broom"></i><span>Clear Filter</span></button>
                    </div>
                </div>
                <div class="product-bar"><h6>Filter by Categories</h6>
                    <div class="product-bar-content"><input type="text" placeholder="Search">
                        <ul class="nasted-dropdown" id="shop_page_cate_list">

                        </ul>
                        <button class="clear-filter-btn"><i class="fas fa-broom"></i><span>Clear Filter</span></button>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="product-filter">
                            <div class="product-page-number"><p>Showing 1–9 of 130 results</p></div>
                            <select class="product-short-select custom-select">
                                <option selected>Short by Best Sell</option>
                                <option value="1">Short by New Item</option>
                                <option value="2">Short by Popularity</option>
                                <option value="3">Short by Average review</option>
                            </select>
                            <ul class="product-card-type">
                                <li class="grid-verti active"><i class="fas fa-grip-vertical"></i></li>
                                <li class="grid-hori"><i class="fas fa-grip-horizontal"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row product-card-parent" id="shop_page_product_list"
                     style="height: 1000px;overflow: scroll;">

                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#"><i
                                            class="fas fa-long-arrow-alt-left"></i></a></li>
                            <li class="page-item"><a class="page-link active" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">...</li>
                            <li class="page-item"><a class="page-link" href="#">67</a></li>
                            <li class="page-item"><a class="page-link" href="#"><i
                                            class="fas fa-long-arrow-alt-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
