function get_key_value(key, endpoint) {
    var formdata = new FormData;
    formdata.append('action', 'key_retrieve');
    formdata.append('name', key);
    return $.ajax(endpoint, {
        type: 'POST',
        data: formdata,
        processData: false,
        contentType: false,
        async: false,
        success: function (res) {
        },
        error: function (error) {
            alert('we have an error!');
        }
    })
}

function save_key_values(name, value, endpoint) {
    var formdata = new FormData;
    formdata.append('action', 'key_entry');
    formdata.append('name', name);
    formdata.append('value', value);
    $.ajax(endpoint, {
        type: 'POST',
        data: formdata,
        processData: false,
        contentType: false,
        success: function () {
            alert('You have successfully updated the following key: ' + name);
        },
        error: function () {
            alert('we have an error!');
        }
    })
}