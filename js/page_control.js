function set_all_invisible() {
    document.getElementById('home').style.display = 'none';
    document.getElementById('contact').style.display = 'none';
    document.getElementById('cart').style.display = 'none';
    document.getElementById('products').style.display = 'none';
    document.getElementById('about').style.display = 'none';
    document.getElementById('login').style.display = 'none';
    document.getElementById('wish').style.display = 'none';
    document.getElementById('account').style.display = 'none';
}

function change_page(name) {
    set_all_invisible();
    if (name === 'home') {
        document.getElementById('home').style.display = 'block';
    } else if (name === 'contact') {
        document.getElementById('contact').style.display = 'block';
    } else if (name === 'cart') {
        document.getElementById('cart').style.display = 'block';
    } else if (name === 'products') {
        document.getElementById('products').style.display = 'block';
    } else if (name === 'about') {
        document.getElementById('about').style.display = 'block';
    } else if (name === 'login') {
        document.getElementById('login').style.display = 'block';
    } else if (name === 'wish') {
        document.getElementById('wish').style.display = 'block';
    } else if (name === 'account') {
        document.getElementById('account').style.display = 'block';
    }
}
